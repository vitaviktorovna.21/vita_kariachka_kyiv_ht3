﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;

namespace NewProgectTests.PageObjects
{
    class MainMenuPageObject : BasePage
    {
       
        [FindsBy(How = How.XPath, Using = "//div[@class='header-bottom__login flex-wrap middle-xs']//div[@class='header-bottom__right-icon']")]
        public IWebElement SignInButton { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//div[@class='col-xs-12 js_message']")]
        public IWebElement UserLogin { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='input_search']")]
        public IWebElement SearchInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='button-reset search-btn']")]
        public IWebElement GoToSearchResault { get; set; }

        [FindsBy(How = How.XPath, Using = "//main/section[1]//div/div/ul/li[8]")]
        public IWebElement ItemButtonGadget { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='mCSB_1_container']/div/h2")]
        public IWebElement MessageOrderGadget { get; set; }

        public MainMenuPageObject(IWebDriver webdriver): base(webdriver) { }
    }
}
