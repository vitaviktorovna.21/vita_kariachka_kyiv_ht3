﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace NewProgectTests.PageObjects
{
    class ResaultSearchPage : BasePage
    {
        public ResaultSearchPage(IWebDriver webdriver) : base(webdriver) { }

        [FindsBy(How = How.XPath, Using = "//div[@class='prod-cart height']")]
        public IList<IWebElement> ResaultBySearch { get; set; }
    }
}
