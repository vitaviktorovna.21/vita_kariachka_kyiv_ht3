﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace NewProgectTests.PageObjects
{
    public class BasePage
    {
        protected IWebDriver webDriver;
       
        public BasePage(IWebDriver webdriver)
        {
            PageFactory.InitElements(webdriver, this);
        }
    }
}
