﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace NewProgectTests.PageObjects 
{
    class AutorizationPageObject : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//span[text()='Номер телефону або email']//following-sibling::div/input[@type='text'][@name='login']")]
        public IWebElement LoginInputButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='validate show-password']")]
        public IWebElement PasswordInputButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='button-reset main-btn submit main-btn--green']")]
        public IWebElement EnterButton  { get; set;}
        
        public AutorizationPageObject(IWebDriver webdriver) : base (webdriver) { }
    }
}
