using System;
using System.Linq;
using NewProgectTests.PageObjects;
using NewProgectTests.PageTests;
using NUnit.Framework;


namespace NewProgectTests
{
    public class BaseFunctionalityTests : BaseTest
    {
        [Test]
        public void CheckAvtorization1()
        {
            var mainMenuPage = new MainMenuPageObject(_webdriver);
            mainMenuPage.SignInButton.Click();
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            var autorizationPage = new AutorizationPageObject(_webdriver);
            autorizationPage.LoginInputButton.SendKeys(TestSettings.StartLogin);
            autorizationPage.PasswordInputButton.SendKeys(TestSettings.StartLoginPassword);
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            autorizationPage.EnterButton.Click();
            Assert.AreEqual(mainMenuPage.UserLogin.Text, TestSettings._expectedLogin);
        }
        [Test]
        public void CheckThatUrlContainsSearchWord()
        {
            var mainMenuPage = new MainMenuPageObject(_webdriver);
            mainMenuPage.SearchInput.SendKeys("iphone 13");
            mainMenuPage.GoToSearchResault.Click();
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            Assert.IsTrue(_webdriver.Url.Contains("query=iphone"));
        }
        [Test]
        public void GadgetPage_Should_ContainsCorrectData()
        {
            var expected = "������ �������";
            var mainMenuPage = new MainMenuPageObject(_webdriver);
            mainMenuPage.ItemButtonGadget.Click();
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            Assert.AreEqual(mainMenuPage.MessageOrderGadget.Text, expected);
        }
        [Test]
        public void CheckElementsAmountOnSearchPage()
        {
            var mainMenuPage = new MainMenuPageObject(_webdriver);
            mainMenuPage.SearchInput.SendKeys("�������");
            mainMenuPage.GoToSearchResault.Click();
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            var elementList = new ResaultSearchPage(_webdriver);
            var actualElementsSize = elementList.ResaultBySearch.Count();
            Assert.AreEqual(actualElementsSize, 12);
        }
    }
}




           