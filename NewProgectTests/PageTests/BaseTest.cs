﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace NewProgectTests.PageTests
{
    public class BaseTest
    {
        protected IWebDriver _webdriver;

        [TearDown]
        protected void DoAfterEach()
        {
            _webdriver.Quit();
        }

        [SetUp]
        protected void DoBeforeEach()
        {
            _webdriver = new ChromeDriver();
            _webdriver.Manage().Cookies.DeleteAllCookies();
            _webdriver.Navigate().GoToUrl(TestSettings.HostPrefix);
            _webdriver.Manage().Window.Maximize();
        }
    }
}
